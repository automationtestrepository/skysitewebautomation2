package com.skysite.testscripts;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.arcautoframe.utils.EmailReport;
import com.arcautoframe.utils.Log;
import com.arcautoframe.utils.RetryAnalyzer;
import com.arcautoframe.utils.WebDriverFactory;
import com.skysite.pages.HomePage;
import com.skysite.pages.LoginPage;
import com.skysite.pages.SplashPage;
import com.skysite.utils.PropertyReader;

import com.skysite.utils.SkySiteAppLaunch;

@Listeners(EmailReport.class)

public class SkySiteRegressionSuite1{
	
	//WebDriver driver;
	String webSite;
	ITestResult result;
	SplashPage splashPage;
	LoginPage loginPage;
	HomePage homePage;
	
	
	/** TC_01: Verify successful Application launch.
	 * 
	 * @throws Exception
	 */
	@Test(priority = 0, enabled = true, description = "TC_01: Verify successful Application launch.")
	public void verifyAppLaunch() throws Exception
	{
		Log.testCaseInfo("Verify successful Application launch.");
		WebDriver driver = SkySiteAppLaunch.launchURL();
		Log.message("Browser Launched and maximized successfully");
		try
		{
			splashPage = new SplashPage(driver).get();
			Log.message("Splash page objects initiated successfully");
			Log.assertThat(splashPage.skySiteApplaunch(), "SkySite application lanched successfully.", "SkySite application does not launch successfully.");
		}
		catch(Exception e)
		{
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_02: Verify successful landing of Login Page.
	 * @throws Exception 
	 * 
	 */
	@Test(priority = 1, enabled = false, description = "TC_02: Verify successful landing of Login Page.")
	public void verifyLandingOfLoginPage() throws Exception
	{
		Log.testCaseInfo("Verify successful landing of Login Page.");
		WebDriver driver = SkySiteAppLaunch.launchURL();
		Log.message("Browser Launched and maximized successfully");
		try
		{
			splashPage = new SplashPage(driver).get();
			loginPage = splashPage.clickOnSignIn();
			Log.message("Sign In button is clicked");
			Log.assertThat(loginPage.presenceOfTextBoxUserName(), "LogIn page landed successfully.", "LogIn page is not landed successfully.");
		}
		catch(Exception e)
		{
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_03: Verify login with valid credential.
	 * 
	 */
	@Test(priority = 2, enabled = false, description = "TC_03: Verify login with valid credential.")
	public void verifyLoginWithValidCredential() throws Exception
	{
		Log.testCaseInfo("Verify successful landing of Project Creation Page.");
		WebDriver driver = SkySiteAppLaunch.launchURL();
		Log.message("Browser Launched and maximized successfully");
		try
		{
			splashPage = new SplashPage(driver).get();
			loginPage = splashPage.clickOnSignIn();
			homePage = loginPage.loginWithValidCredential();
			Log.assertThat(homePage.presenceOfProfileButton(), "Login successful with valid credential.", "Login unsuccessful with valid credential." );
		}
		catch(Exception e)
		{
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_04: Verify login with invalid credential.
	 * 
	 */
	@Test(priority = 3, enabled = false, description = "TC_04: Verify login with invalid credential.")
	public void verifyLoginWithInvalidCredential() throws Exception
	{
		Log.testCaseInfo("Verify Login would be failed with Invalid credentials.");
		WebDriver driver = SkySiteAppLaunch.launchURL();
		Log.message("Browser Launched and maximized successfully");
		try
		{
			splashPage = new SplashPage(driver).get();
			loginPage = splashPage.clickOnSignIn();
			Log.assertThat(loginPage.loginWithInvalidCredential(), "Login unsuccessful with invalid credential", "Login successful with invalid credential.");
		}	
		catch(Exception e)
		{
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
	
	/** TC_05: Verify project creation.
	 * 
	 */
	@Test(priority = 4, enabled = false, description = "TC_04: Verify project creation.")
	public void verifyProjectCreation() throws Exception
	{
		Log.testCaseInfo("Verify project creation.");
		WebDriver driver = SkySiteAppLaunch.launchURL();
		Log.message("Browser Launched and maximized successfully");
		try
		{
			splashPage = new SplashPage(driver).get();
			loginPage = splashPage.clickOnSignIn();
			homePage = loginPage.loginWithValidCredential();
			homePage.presenceOfProfileButton();
			Log.assertThat(homePage.createProject(), "Project created successfully.", "Project creation failed");
		}
		catch(Exception e)
		{
			e.getCause();
			Log.exception(e, driver);
		}
		finally
		{
			Log.endTestCase();
			driver.quit();
		}
	}
}

package com.skysite.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.arcautoframe.utils.Log;
import com.skysite.utils.PropertyReader;
import com.skysite.utils.SkySiteUtils;

public class LoginPage extends LoadableComponent<LoginPage> {

	WebDriver driver;
	private boolean isPageLoaded;
	
	@FindBy(css="#UserID")
	WebElement txtBoxUserName;
	
	@FindBy(css="#Password")
	WebElement txtBoxPassword;
	
	@FindBy(css="#btnLogin")
	WebElement btnLogin;
	
	@Override
	protected void load() {
		isPageLoaded = true;
		SkySiteUtils.waitForElement(driver, txtBoxUserName, 20);
		
	}

	@Override
	protected void isLoaded() throws Error {
		if (!isPageLoaded) {
			Assert.fail();
		}
	}
	
	public LoginPage(WebDriver driver) {
		   this.driver = driver;
		   PageFactory.initElements(this.driver, this);
		 }
	
	/** Checking whether User Name text box is present?
	 * 
	 * @return
	 */
	public boolean presenceOfTextBoxUserName()
	{
		SkySiteUtils.waitForElement(driver, txtBoxUserName, 20);
		Log.message("Waiting for Username text box to be appeared");
		if(txtBoxUserName.isDisplayed())
			return true;
			else
			return false;
	}
	
	/** Login with valid credential.
	 * 
	 */
	public HomePage loginWithValidCredential()
	{
		SkySiteUtils.waitForElement(driver, txtBoxUserName, 20);
		String uName = PropertyReader.getProperty("Username");
		txtBoxUserName.clear();
		txtBoxUserName.sendKeys(uName);
		Log.message("Username: " + uName + " " + "hasa been engtered in Username text box." );
		String pWord = PropertyReader.getProperty("Password");
		txtBoxPassword.clear();
		txtBoxPassword.sendKeys(pWord);
		Log.message("Password: " + pWord + " " + "hasa been engtered in Password text box." );
		btnLogin.click();
		Log.message("LogIn button clicked.");
		return new HomePage(driver).get();
		
	}
	
	/** Login with invalid credential.
	 * 
	 * @return
	 */
	public boolean loginWithInvalidCredential()
	{
		SkySiteUtils.waitForElement(driver, txtBoxUserName, 20);
		String uName = PropertyReader.getProperty("Invalidusername");
		txtBoxUserName.clear();
		txtBoxUserName.sendKeys(uName);
		Log.message("Username: " + uName + " " + "hasa been engtered in Username text box." );
		String pWord = PropertyReader.getProperty("Invalidpassword");
		txtBoxPassword.clear();
		txtBoxPassword.sendKeys(pWord);
		Log.message("Password: " + pWord + " " + "hasa been engtered in Password text box." );
		btnLogin.click();
		Log.message("LogIn button clicked.");
		SkySiteUtils.waitTill(5000);
		String actualTitle = "Sign in - SKYSITE";
		String expectedTitle = driver.getTitle();
		if(actualTitle.equalsIgnoreCase(expectedTitle))
			return true;
		else
			return false;
	}

}

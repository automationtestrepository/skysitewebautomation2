package com.skysite.utils;

import java.io.IOException;

import org.openqa.selenium.WebDriver;

import com.arcautoframe.utils.WebDriverFactory;

public class SkySiteAppLaunch {
	
	WebDriver driver;
	
	public static WebDriver launchURL() throws IOException
	{
		WebDriverFactory webfac = new WebDriverFactory();
		return webfac.browserLaunch(PropertyReader.getProperty("BrowserName"), PropertyReader.getProperty("SkysiteProdURL"));
		//return driver;
	}

}
